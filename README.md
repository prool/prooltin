# prooltin

ProolTin - Prool's fork of tintin++ MUD client

Based on tintin++ 2.01.7

Main features:
- watchdog (f.e., for using in shell scripts)
- #proolcoder - similar to codetable UTF8TOKOI8 in [modern tintin++](http://tintin.mudhalla.net/)
- simplest translator from English to Russian (pre-beta, under construction, many bugs)

How to make in Android
----------------------

cp tintin++/src

./configure

manually add flag -liconv to Makefile, parameter LIBS

make

How to make in macOS
--------------------

cp tintin++/src

./configure

make -f Makefile.macos

How to make in Windows/cygwin
-----------------------------

cp tintin++/src

./configure

manually add flag -liconv to Makefile, parameter LIBS

make -f Makefile

[I'm making Prooltin binaryes for Windows](http://files.mud.kharkov.org/tintin/prooltin.win/)

How to make in others systems
-----------------------------

cp tintin++/src

./configure

make
