DO_COMMAND(do_prool);
DO_COMMAND(do_tron);
DO_COMMAND(do_troff);
DO_COMMAND(do_proolwatchdogtimer);
DO_COMMAND(do_totalon);
DO_COMMAND(do_totaloff);
DO_COMMAND(do_addword);
DO_COMMAND(do_delword);
DO_COMMAND(do_listdic);
DO_COMMAND(do_writedic);
DO_COMMAND(do_mouseon);
DO_COMMAND(do_mouseoff);
DO_COMMAND(do_proolcoder_switch);
DO_COMMAND(do_muller);
DO_COMMAND(do_ls);
DO_COMMAND(do_cat);
DO_COMMAND(do_chdir);
DO_COMMAND(do_translate_word);

char *prooltran(char *si);
void prool_log(char *message);
void prool_ident(void);
char *ptime(void);
void uptime_(void);
void prooltranslate_init(int greeting);
int prool_loop(void);

void enable_mouse(void);
void disable_mouse(void);

void koi_to_utf8(char *str_i, char *str_o);
void utf8_to_koi(char *str_i, char *str_o);
void init_terminal_from_tintin(void);
void restore_terminal_from_tintin(void);

extern int total_log;
extern int tron;
extern int coder;
extern long int start_time;
extern long int startwatchtime;
extern int prool_loop_counter;
extern long int watchdog;
extern FILE *muller_fp;

#define ESC 033
#define ESC_STR "\033"
#define NORM_COLOR "\033[0m"

#define KRASN "\033[31m"
#define ZELEN "\033[32m"
#define ZHELT "\033[33m"
#define SIN "\033[34m"
#define FIOL "\033[35m"
#define GOLUB "\033[36m"
#define BEL "\033[37m"

#define KRASN1 "\033[1;31m"
#define ZELEN1 "\033[1;32m"
#define ZHELT1 "\033[1;33m"
#define SIN1 "\033[1;34m"
#define FIOL1 "\033[1;35m"
#define GOLUB1 "\033[1;36m"
#define BEL1 "\033[1;37m"

#define HALF "\033[2m"
#define UNDERSCORE "\033[4m"
#define BLINK "\033[5m"
#define REVERSE "\033[7m"
